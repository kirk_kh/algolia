<?php
/*
Plugin Name: Algolia Plugin
Plugin URI: http://apps.phpfoxer.net/dev/wp/
Description:Algolia Plugin
Version: 1.0
Author: phpfoxer
Author URI: http://apps.phpfoxer.net/dev/wp/
Text Domain: algolia
License: GPLv2 or later
*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class algolia {

	private static $instance;
	
	private function __construct() {

		$this->constants();
		$this->includes();
		$this->actions();
	}
	
	public static function init()
    {
        if(!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }
	
	function constants()
	{
		global $wpdb;
		
		define('ALG_PLUGIN_FILE',  __FILE__ );
		define('ALG_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		define('ALG_PLUGIN_URL', plugins_url( '/', __FILE__ ) );

	}
	
	function includes()
	{
		require_once( ALG_PLUGIN_DIR. 'functions.php');
	}
	
	function actions()
	{
		register_activation_hook(__FILE__ , array($this, 'alg_plugin_activate' ) );
		register_deactivation_hook(__FILE__ , array($this, 'alg_plugin_deactivate' ) );
	}

   	function am_plugin_activate()
   	{
		global $wpdb;		
	}
	
	function am_plugin_deactivate()
	{
	
	}
}
algolia::init();