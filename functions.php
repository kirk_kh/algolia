<?php
	add_action('wp_enqueue_scripts', 'alg_enqueue_scripts');

	// function alg_enqueue_scripts()
	// {
	// 	wp_register_style( 'alg-style', ALG_PLUGIN_URL . 'assets/css/style.css' );
	// 	wp_enqueue_style( 'alg-style' );

	// 	wp_register_script( 'alg-common', ALG_PLUGIN_URL . 'assets/js/common.js', array( 'jquery' ), '3.1', true );
	// 	wp_localize_script( 'alg-common', 'alg_common', 
	// 		array( 
	// 			'ajaxurl' => admin_url( 'admin-ajax.php' )
	// 		) 
	// 	);
	// 	wp_enqueue_script('ab-common');
	// }

	function alg_enqueue_scripts() {
		wp_enqueue_script( 'bundle', ALG_PLUGIN_URL . '/dist/bundle.js', array('jquery'), 1, false );
	}
	
	add_action('admin_menu', 'alg_add_pages');

	function alg_add_pages()
	{
		add_menu_page('Algolia Plugin', 'Algolia', 'manage_options', 'algolia-menu','alg_admin_template','dashicons-welcome-view-site');
	}

	function alg_admin_template()
	{
		require_once( ALG_PLUGIN_DIR. 'templates/alg-admin.php' );		
	}

	add_action('admin_head', 'wph_inline_css_admin');

	function wph_inline_css_admin() {
	echo '<style>
		.ais-hits
		{
			padding:10px;
			border: 1px solid #cccccc;
			border-radius: 5px;
			margin: 5px;
			-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
			box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
			background-color:white;
		}
		.ais-hits--item
		{
			font-weight: 600;
		    font-family: cursive;
		    padding: 5px;
		    text-decoration: underline;
		    text-shadow: 1px 2px 2px rgba(150, 150, 150, 1);
		    cursor:help;
		    letter-spacing:2px;			
		}
		.ais-hits--item:hover
		{
			color:#000000;
			font-size:16px;
		}
		.ais-hits--item:first-child
		{
			background: #CCC;
		}
		.ais-hits--item:nth-child(2n+3)
		{
			background: #CCC;
		}
	 </style>';
	}
	