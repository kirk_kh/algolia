<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.1.4/dist/instantsearch.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/instantsearch.js@2.1.4/dist/instantsearch-theme-algolia.min.css">
<script src="https://cdn.jsdelivr.net/npm/instantsearch.js@2.1.4/dist/instantsearch.min.js"></script>
<h2>Test Algolia Plugin</h2>
<div class="row">
	<div class="col-md-12 col-lg-12">
		<div id="current-refined-values">
		  <!-- CurrentRefinedValues widget will appear here -->
		</div>

		<div id="clear-all">
		  <!-- ClearAll widget will appear here -->
		</div>
		<div id="search-box">
		  <!-- SearchBox widget will appear here -->
		</div>

		<div id="refinement-list">
		  <!-- RefinementList widget will appear here -->
		</div>

		<div id="hits">
		  <!-- Hits widget will appear here -->
		</div>

		<div id="pagination">
		  <!-- Pagination widget will appear here -->
		</div>
	</div>
</div>
<script>
	  const search = instantsearch({
		  appId: 'latency',
		  apiKey: '6be0576ff61c053d5f9a3225e2a90f76',
		  indexName: 'instant_search',
		  urlSync: true
		});
	  // initialize currentRefinedValues
	  search.addWidget(
	    instantsearch.widgets.currentRefinedValues({
	      container: '#current-refined-values',
	      // This widget can also contain a clear all link to remove all filters,
	      // we disable it in this example since we use `clearAll` widget on its own.
	      clearAll: false
	    })
	  );

	  // initialize clearAll
	  search.addWidget(
	    instantsearch.widgets.clearAll({
	      container: '#clear-all',
	      templates: {
	        link: 'Reset everything'
	      },
	      autoHideContainer: false
	    })
	  );

	  // initialize pagination
	  search.addWidget(
	    instantsearch.widgets.pagination({
	      container: '#pagination',
	      maxPages: 20,
	      // default is to scroll to 'body', here we disable this behavior
	      scrollTo: false
	    })
	  );
	  // initialize RefinementList
	  search.addWidget(
	    instantsearch.widgets.refinementList({
	      container: '#refinement-list',
	      attributeName: 'categories'
	    })
	  );

	  // initialize SearchBox
	  search.addWidget(
	    instantsearch.widgets.searchBox({
	      container: '#search-box',
	      placeholder: 'Search for products'
	    })
	  );

	  // initialize hits widget
	  search.addWidget(
	    instantsearch.widgets.hits({
	      container: '#hits',
	      templates: {
	        empty: 'No results',
	        item: '<em>Hit {{objectID}}</em>: {{{_highlightResult.name.value}}}'
	      }
	    })
	  );

	  search.start();
</script>